from selenium.webdriver.common.by import By


# for maintainability we can seperate web objects by page name

class MainPageLocators(object):
    # LOGO = (By.CLASS_NAME,'logo-row ')
    # SCHEDULE = [(By.CLASS_NAME,'schedule-button')]
    # ACCOUNT = (By.ID, 'nav-link-accountList')
    # SIGNUP = (By.CSS_SELECTOR, '#nav-signin-tooltip > div > a')
    # LOGIN = (By.CSS_SELECTOR, '#nav-signin-tooltip > a')
    # CHANGELANGUAGE = (By.ID, 'twotabsearchtextbox')
    # SEARCH_LIST = (By.CSS_SELECTOR, 'div[data-component-type="s-search-result"]')


 card = (By.CSS_SELECTOR,'div.inner-wrapper div.main-section div.contents-section div.event-card.card:nth-child(2) div.card-footer > button.schedule-button')
 logo = (By.XPATH,'//*[@alt="Corporate logo"]')
 countryDropDown=(By.ID,'country')
 countryIndia=(By.ID,'country_0')
 countryUSA=(By.ID,'country_1')
 eventFirst=(By.ID,'event-66905')
 selectBOX = (By.XPATH, 'Select')
 scheduleBOX =[(By.XPATH, '//*[@translate="SCHEDULE"]')]
 gotonextmonth=(By.XPATH,'//*[@aria-describedby="tooltip-04fa7"]')
 selectDate=(By.XPATH,'//*[text()="20"]')
 selectTime=[(By.XPATH,'//*[@tabindex="0"]')]
 confirmBOX = (By.XPATH, '//*[@data-container="confirm-button"]')
 chatBOX = (By.CLASS_NAME, 'chatBot-box onlyDesktop')
 faq = (By.CLASS_NAME, 'faq-box onlyDesktop')
 normalFONT = (By.TAG_NAME, 'Normal font')
 largeFONT = (By.TAG_NAME, 'Large font')
 # language1 = (By.TAG_NAME, 'English')
 # language2 = (By.TAG_NAME, 'Français')
 # language3 = (By.TAG_NAME, 'Português')
 # language4 = (By.TAG_NAME, 'Español')
 # language5 = (By.TAG_NAME, '普通话')
 # country1 = (By.TAG_NAME, 'India')
 # country2 = (By.TAG_NAME, 'United States')
 activitySCHEDULE = (By.TAG_NAME, 'Schedule')
 CAUSES = (By.CLASS_NAME, 'causes')
 causeCLOSE = (By.TAG_NAME, 'Close')
 chatICON = (By.CLASS_NAME, 'chat-content')
 ChatCLOSE = (By.CLASS_NAME, 'd_hotline minimize ')
 OpenFontchange=(By.XPATH,'//*[@alt="Open dropdown"]')
 NormalFont =(By.ID,'zoom-desktop_0_text')
 LargeFONT=(By.ID,'zoom-desktop_1_text')
 IndiaFirstEventName=[(By.XPATH,'//*[@class="name"]')]
 LanguageDropDown=(By.ID,'language-desktop')
 LanguageFrancais=(By.ID,'language-desktop_1')
 LanguageChinese=(By.ID,'language-desktop_4')
 Faq=(By.XPATH,'//*[@class="faq-box onlyDesktop"]')
 chatbox=(By.XPATH,'//*[@class="chatBot-box onlyDesktop"]')
 ReplyHere=(By.XPATH,'//*[@placeholder="Reply here..."]')
 ExpandCard=(By.XPATH,'//*[@alt="Click here to see more details"]')
 TagListonEVENT=[(By.XPATH,'//*[@class="tag-text"]')]
 EventDescription=(By.XPATH,'//*[@class="description"]')
 NumberofVolunteersNeeded=(By.XPATH,'//*[@alt="Number of volunteers needed"]')
 Timecommitment=(By.XPATH,'//*[text()="Approx 60 mins"]')





 ###### start of add emcee variables

 EmailLogin=(By.ID,'email')
 PasswordLogin=(By.ID,'password')
 RemembermeToggle=(By.ID,'remember me')
 SubmitLogin=(By.XPATH,'//*[@type="submit"]')
 LoginSuccessBanner=(By.XPATH,'//*[@role="alert"]')
 AddEmcee=(By.XPATH,'//*[text()="Add Emcee"]')
 SelectEmceeTab=(By.XPATH,'//*[text()="EMCEE"]')
 UploadEmceeImage=(By.XPATH,'//*[@type="file"]')
 SubmitEmceeImage = (By.ID,'upload')
 ImageUploadedSuccessBannerEmcee = (By.XPATH, '//*[@role="alert"]')
 EmceeName=(By.ID,'name')
 PrimaryEmail=(By.ID,'email')
 AddSecondaryEmailtoggle=(By.XPATH,'//*[text()="+ Add secondary email"]')
 AddSecondaryEmail=(By.ID,'email#0')
 CloseSecondaryEmail=(By.XPATH,'//*[@class="p-2"]')
 EmceeAbout = (By.XPATH, '//*[@id="about"]')
 Selectcategory_Emcee = (By.XPATH,'//div[@aria-labelledby="Event category"]')
 Eventcategory_Emcee = (By.XPATH,'//*[text()="Audiobook Recording"]')
 Selectcountry_Emcee = (By.XPATH, '//div[@aria-labelledby="Select country"]')
 Searchcountry_Emcee = (By.ID, 'Select country')
 SelectcountryAfghanistan = (By.XPATH,'//*[text()="Afghanistan"]')
 SelectcountryAlgeria = (By.XPATH,'//*[text()="Algeria"]')
 DeselectcountryAlgeria = (By.XPATH,'//div[@aria-labelledby="Select country"]/div[1]/div[2]/button[@aria-label="remove"]')
 EventCountryUSA =(By.XPATH,'//*[text()="United States of America"]')
 Selectlanguage_Emcee = (By.XPATH, '//div[@aria-labelledby="Select language"]')
 Searchlanguage_Emcee = (By.ID, 'Select language')
 EventLanguageEnglish_Emcee = (By.XPATH, '//*[text()="English"]')
 SubmitEmcee=(By.XPATH,'//*[@type="submit"]')
 DeleteImage=(By.XPATH, '//*[text()="Delete"]')
 SelectcountryBhutan_Emcee = (By.XPATH, '//*[text()="Bhutan"]')
 EventLanguageFrench_Emcee = (By.XPATH, '//*[text()="French"]')
 EventLanguageHindi_Emcee = (By.XPATH, '//*[text()="Hindi"]')
 ExistingEmcee = (By.XPATH, '//*[@id="__next"]/div[2]/main/ol/li[1]/a/div')
 EmceeSuccessfullySavedBanner = (By.XPATH, '//*[@role="alert"]')
 EmceeSuccessfullyAddedBanner = (By.XPATH, '//*[@role="alert"]')


##### start of add event variables
 AddEvent = (By.XPATH, '//*[text()="ADD EVENT"]')
 Eventname=(By.ID,'name')
 ParticipantFormatGroup=(By.XPATH,'//*[@id="Group"]')
 ParticipantFormatIndividual=(By.XPATH,'//*[@id="Individual"]')
 EventType_Event=(By.ID,"Event type")
 SelectAudioRecording=(By.XPATH,'//*[@role="option"]')
 ChangeEventType=(By.XPATH, '//*[@id="Event type"]/button/span[1]/span/span/*[name()="svg"]')
 Searchlanguage_Event = (By.ID, 'Event language')
 Selectlanguage_Event=(By.XPATH, '//div[@aria-labelledby="Event language"]')
 #SelectlanguageArabic= (By.XPATH, '//*[text()="Arabic"]')
 #EventLanguageSpanish = (By.XPATH,'//*[text()="Spanish"]')
 Deselectlanguage = (By.XPATH, '//div[@aria-labelledby="Event language"]/div[1]/div[1]/button[@aria-label="remove"]')
 SelectClient=(By.ID,'Client')
 ChangeClientType = (By.XPATH, '//*[@id="Client"]/button/span[1]/span/span/*[name()="svg"]')
 Searchcountries = (By.ID, 'Event country')
 Selectcountry = (By.XPATH, '//div[@aria-labelledby="Countries"]')
 EventCounrtySpain = (By.XPATH, '//*[text()="Spain"]')
 Deselectcountry = (By.XPATH, '//div[@aria-labelledby="Countries"]/div[1]/div[1]/button[@aria-label="remove"]')
 #EventNonProfit = (By.XPATH, '//*[text()="Test partner 2.0"]')
 SelectNonProfit=(By.ID,'Non-profit')
 ChangeNonProfit=(By.XPATH, '//*[@id="Non-profit"]/button/span[1]/span/span/*[name()="svg"]')
 ChampionsPortalNo=(By.ID,'No')
 MaxVolunteer=(By.ID,'maxVol')
 MinVolunteer=(By.ID,'minVol')
 PasteCalendlylink=(By.ID,'calendlyLink')
 UploadEventImage = (By.XPATH, '//*[@type = "file"]')
 SubmitEventImage = (By.ID, 'upload')
 ImageUploadedSuccessBanner=(By.XPATH,'//*[@role="alert"]')
 EventDescription=(By.ID,'description')
 EventPrereq=(By.XPATH,'//*[@id="prerequisites"]')
 WhyVolunteer=(By.ID,'whyVolunteer')
 SelectCauses = (By.XPATH, '//div[@aria-labelledby="Causes"]')
 SearchCauses=(By.ID,'Causes')
 SelectCauseEnvironment=(By.XPATH,'//*[text()="Environment"]')
 SelectCauseAnimalWelfare = (By.XPATH, '//*[text()="Animal welfare"]')
 SelectCauseEducation = (By.XPATH, '//*[text()="Education"]')
 DeSelectCauses= (By.XPATH, '//*[@id="__next"]/div[2]/div/div[2]/form/div[15]/div/div/div/div[3]/button')
 SelectBeneficiaries=(By.XPATH, '//div[@aria-labelledby="Beneficiaries"]')
 SelectBeneficiaryAdults=(By.XPATH, '//*[text()="Adults"]')
 SelectBeneficiaryPatients = (By.XPATH, '//*[text()="Patients"]')
 EnterItineraryDuration1 =(By.ID,'duration-0')
 EnterItineraryTitle1 =(By.ID,'title-0')
 AddItinerary = (By.XPATH, '//*[text()="+ Add More"]')
 EnterItineraryDuration2 = (By.ID, 'duration-1')
 EnterItineraryTitle2 = (By.ID, 'title-1')
 DeleteItineraryDetails=(By.XPATH,'//*[@id="__next"]/div[2]/div/div[2]/form/div[17]/div/ol/li[1]/button/span[1]/*[name()="svg"]')
 DeckFileUploadedSuccessBanner=(By.XPATH,'//*[@role="alert"]')
 UploadDeckFile=(By.XPATH,'//*[text()="Click here to upload a deck file"]')
 ConfirmDeckFile=(By.XPATH,'//*[text()="Done"]')
 AttachDeckFile=(By.XPATH,'//*[text()="Drop file here or click to upload"]')
 AddHashTags=(By.ID,'hashtags')
 Cancelbutton=(By.XPATH,'//*[@type="reset"]')
 Submitbutton=(By.XPATH,'//*[@type="submit"]')
 EventCreatedSuccessBanner=(By.XPATH,'//*[@role="alert"]')





###Start of event listing variables ###

 Event = (By.XPATH, '//*[text()="EVENT"]')
 SearchEventName= (By.ID,'name')
 SearchButton= (By.XPATH,'//*[@type="submit"]')
 # LoadMoreButton = (By.XPATH, '//*[@text="Load More"]')
 LoadMoreButton= (By.XPATH, '//*[@id="__next"]/div[2]/div/section/div/main/div[2]/div[2]/button')
 SearchClient= (By.XPATH, '//div[@aria-labelledby="Client"]')
 TestClient = (By.XPATH, '//*[text()="Test"]')
 SearchCountries=(By.XPATH, '//div[@aria-labelledby="Country"]')
 Selectcountry1 = (By.XPATH, '//*[text()="Afghanistan"]')
 Selectcountry2 = (By.XPATH, '//*[text()="United States of America"]')
 Selectcountry3 = (By.XPATH, '//*[text()="India"]')
 Deselectcountry3 = (By.XPATH, '//div[@aria-labelledby="Country"]/div[1]/div[3]/button[@aria-label="remove"]')
 SearchNonProfit= (By.XPATH, '//div[@aria-labelledby="Non-profit"]')
 DeselectNonProfit = (By.XPATH, '//div[@aria-labelledby="Non-profit"]/div[1]/div[1]/button[@aria-label="remove"]')
 TestPartner = (By.XPATH, '//*[text()="Test partner"]')
 TestPartner2 = (By.XPATH, '//*[text()="Test partner 2.0"]')
 SearchEventType= (By.XPATH, '//div[@aria-labelledby="Event type"]')
 AudioBookRecording = (By.XPATH, '//*[text()="Audiobook Recording"]')
 ChooseEvents = (By.XPATH, '//*[@id="Choose Events"]/button')
 AllEventTab= (By.XPATH,'//*[text()="All Events"]')
 AllSlotsTab= (By.XPATH,'//*[text()="All Slots"]')
 ActiveEventsTab= (By.XPATH,'//*[text()="Active Events"]')
 InactiveEventsTab= (By.XPATH,'//*[text()="Inactive Events"]')
 ChooseSlots = (By.XPATH, '//*[@id="Choose Slots"]/button')
 upcomingSlots= (By.XPATH,'//*[text()="Upcoming Slots"]')
 CompletedSlots= (By.XPATH,'//*[text()="Completed Slots"]')
 ThreeDotsMenuButton = (By.XPATH,'//*[@id="menu-button--menu--1"]')
 EditEventDetails = (By.XPATH, '//*[@id="option-1--menu--1"]/a')
 UpdateEvent=(By.XPATH,'//*[@type="submit"]')
 EventDeckFile= (By.XPATH,'//*[@id="__next"]/div[2]/div/section/div/main/div[2]/div[1]/table/tbody/tr[10]/td[5]/a/span')
 NoDeckFile=(By.XPATH,'//*[@id="__next"]/div[2]/div/section/div/main/div[2]/div[1]/table/tbody/tr[1]/td[5]/span')













 





























class LoginPageLocators(object):
    EMAIL = (By.ID, 'ap_email')
    PASSWORD = (By.ID, 'ap_password')
    SUBMIT = (By.ID, 'signInSubmit-input')
    ERROR_MESSAGE = (By.ID, 'message_error')





