from selenium.webdriver.common.keys import Keys
from pages.base_page import BasePage
from pages.login_page import LoginPage
from pages.signup_page import SignUpBasePage
from utils.locators import *
import time 
from selenium.webdriver.common.by import By
from EventData import *




# Page objects are written in this module.
# Depends on the page functionality we can have more functions for new classes

class MainPage(BasePage):
    def __init__(self, driver):
        self.locator = MainPageLocators
        super().__init__(driver)  # Python3 version

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.logo) else False

    def schedule_event(self):
        self.find_element(*self.locator.scheduleBOX[0]).click()
        time.sleep(5)
        #self.find_element(*self.locator.gotonextmonth).click()
        

        #self.find_element(*self.locator.selectDate[0]).click()
        self.find_element(*self.locator.selectDate).click()
        self.find_element(*self.locator.confirmBOX).click()

        #return self.find_element(*self.locator.SEARCH_LIST).text

    def click_country_change(self):
        self.find_element(*self.locator.countryDropDown).click()
        self.find_element(*self.locator.countryIndia).click()
        eventfirstIndia=self.find_element(*self.locator.IndiaFirstEventName[0]).text
        print(eventfirstIndia =='Create digital cards for underprivileged kids')
        time.sleep(10)
        self.find_element(*self.locator.countryDropDown).click()
        self.find_element(*self.locator.countryUSA).click()
        eventfirstUSA=self.find_element(*self.locator.IndiaFirstEventName[0]).text
        time.sleep(10)
        print(eventfirstUSA =='Create Back to School cards for children')


        #return SignUpBasePage(self.driver)

    def click_font_change(self):
        self.find_element(*self.locator.OpenFontchange).click()
        self.find_element(*self.locator.LargeFONT).click()
        time.sleep(10)
        #return LoginPage(self.driver)

    def click_language_change(self):
        self.find_element(*self.locator.LanguageDropDown).click()
        self.find_element(*self.locator.LanguageFrancais).click()
        eventFirstFrancais=(self.find_element(*self.locator.IndiaFirstEventName[0]).text)
        print(eventFirstFrancais =="Créer des cartes de retour à l'école pour les enfants")
        time.sleep(10)
        self.find_element(*self.locator.LanguageDropDown).click()
        self.find_element(*self.locator.LanguageChinese).click()
        eventFirstChinese=(self.find_element(*self.locator.IndiaFirstEventName[0]).text)
        print(eventFirstChinese =="记录你的职业故事")
    


    def click_FAQ(self):
       
        self.find_element(*self.locator.Faq).click()
        time.sleep(2)
        URL=self.get_url()
        print(URL=="https://volunteer-staging.goodera.com/frequently-asked-questions?fmp=1")


    def click_chat_box(self):
       
        time.sleep(5)
        self.find_element(*self.locator.chatbox).click()
        time.sleep(5)




###### start of AddEmcee methods#########


    def add_EMCEE(self):
        self.driver.get('https://stagingvvapp.goodera.com/')
        self.wait_element(*self.locator.EmailLogin)
        self.find_element(*self.locator.EmailLogin).send_keys(login)
        self.find_element(*self.locator.PasswordLogin).send_keys(password)
        self.find_element(*self.locator.RemembermeToggle).click()
        self.find_element(*self.locator.SubmitLogin).click()
        self.wait_element(*self.locator.SelectEmceeTab)
        self.find_element(*self.locator.SelectEmceeTab).click()
        self.wait_element(*self.locator.AddEmcee)
        self.find_element(*self.locator.AddEmcee).click()
        self.wait_element(*self.locator.EmceeName)
        self.find_element(*self.locator.EmceeName).send_keys(self.generateRandomString(5))

        #self.find_element(*self.locator.UploadEmceeImage).click()
        #self.find_element(*self.locator.UploadEmceeImage).send_keys("/Users/piyush.kumar/desktop/khaby.png")
       
       
        #self.find_element(*self.locator.SubmitEmceeImage).click()
        self.find_element(*self.locator.PrimaryEmail).send_keys(self.generateRandomString(5)+"@goodera.com")
        self.find_element(*self.locator.AddSecondaryEmailtoggle).click()
        self.find_element(*self.locator.AddSecondaryEmail).send_keys(self.generateRandomString(5)+"@goodera.com")
        self.find_element(*self.locator.AddSecondaryEmailtoggle).click()
        self.scrollPageDown()
        self.find_element(*self.locator.CloseSecondaryEmail).click()
        self.scrollPageDown()
        self.find_element(*self.locator.Eventcategory).click()
        self.selectOptionfromDropDown(Eventcategory)
        self.find_element(*self.locator.Selectcountry).click()
        self.selectOptionfromDropDown(EventCountry) 
        self.find_element(*self.locator.Selectlanguage).click()
        self.selectOptionfromDropDown(EventLanguage)
        self.find_element(*self.locator.SubmitEmcee).click()
        time.sleep(20)
        
        
    
    
    
    
    def add_event(self):
        
          self.driver.get('https://stagingvvapp.goodera.com/')
          time.sleep(3)
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(self.GetValueFromJson("login"))
          self.find_element(*self.locator.PasswordLogin).send_keys("Welcome@123")
          self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.ParticipantFormatGroup).click()
          self.find_element(*self.locator.ParticipantFormatIndividual).click()
          self.find_element(*self.locator.EventType).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.EventLanguage).click()
          self.selectOptionfromDropDown("afrikaans")
          self.scrollPageDown()
          self.find_element(*self.locator.SelectClient).click()
          #Xpath of client is same as audi
          self.find_element(*self.locator.SelectAudioRecording).click()
          #self.selectOptionfromDropDown("Test")
          self.find_element(*self.locator.SelectCountries).click()
          self.selectOptionfromDropDown("afghanistan")
          self.scrollPageDown()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          #self.selectOptionfromDropDown("Test partner")
          self.find_element(*self.locator.EventDuration).click()
          #self.selectOptionfromDropDown("30 Minutes")
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.MaxVolunteer).send_keys("200")
          self.find_element(*self.locator.MinVolunteer).send_keys("20")
          self.find_element(*self.locator.PasteCalendlylink).send_keys("https://stagingvvapp.goodera.com/event/add")
          self.scrollPageDown()
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.find_element(*self.locator.EventPrereq).send_keys("test test test ")
          self.find_element(*self.locator.WhyVolunteer).send_keys("test test test ")
          self.scrollPageDown()
          self.find_element(*self.locator.SelectCauses).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.Submitbutton).click()
          time.sleep(10)




          

          




          




          





          



          











   

    


















        




       
        