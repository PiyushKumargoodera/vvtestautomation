from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from pages.base_page import BasePage
from pages.main_page import *
from EventData import *



class EventPage(BasePage):
    def __init__(self, driver):
        self.locator = MainPageLocators
        super().__init__(driver)  # Python3 version
    




    def add_event(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  #necessary
          self.find_element(*self.locator.Eventname).send_keys("Test Event Monday")
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          #Xpath of client is same as audi
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.MinVolunteer).send_keys(minVolunteer)
          self.find_element(*self.locator.MaxVolunteer).send_keys(maxVolunteer)
          self.find_element(*self.locator.PasteCalendlylink).send_keys("https://calendly.com/p3-support/covid-relief-clone-3?utm_campaign=Client")
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.wait_element(*self.locator.EventDescription)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.find_element(*self.locator.EventPrereq).send_keys("test test ")
          self.find_element(*self.locator.WhyVolunteer).send_keys("test test ")
          self.find_element(*self.locator.SelectCauses).click()
          self.find_element(*self.locator.SearchCauses).send_keys(causeEnvironment)
          self.selectOptionfromDropDown2(*self.locator.SelectCauseEnvironment)
          self.find_element(*self.locator.SelectCauses).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          # time.sleep(3)
          self.wait_element(*self.locator.Submitbutton)
          # print(self.find_element(*self.locator.Submitbutton).is_enabled())
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          # print(self.find_element(*self.locator.Submitbutton).is_enabled())
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          # self.find_element(*self.locator.Submitbutton).click()
           # time.sleep(5)
          # self.wait_element(*self.locator.EventCreatedSuccessBanner)


        
    # def VerifyEventChampionsPage(self):
    #     self.add_event()
    #     self.clearCache()
    #     self.driver.get('http://volunteer-client.goodera.com/')
    #     time.sleep(5)
    #     #self.hover(*self.locator.scheduleBOX)
    #     for i in range(3):
    #         self.scrollPageDown()
    #     time.sleep(10)
    #
    #
    # def event_Listing(self):
    #       self.driver.get('https://stagingvvapp.goodera.com/')
    #       self.wait_element(*self.locator.EmailLogin)
    #       self.find_element(*self.locator.EmailLogin).send_keys(login)
    #       self.find_element(*self.locator.PasswordLogin).send_keys(password)
    #     # self.find_element(*self.locator.RemembermeToggle).click()
    #       self.find_element(*self.locator.SubmitLogin).click()
    #       self.wait_element(*self.locator.SearchEventName)
    #       self.find_element(*self.locator.SearchEventName).send_keys("covid")
    #       self.find_element(*self.locator.SearchButton).click()
    #       self.scrollPageDown()
    #       time.sleep(15)

    def remove_MandateFields(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.scrollPageDown()
          self.find_element(*self.locator.MinVolunteer).send_keys(minVolunteer)
          self.find_element(*self.locator.MaxVolunteer).send_keys(maxVolunteer)
          self.find_element(*self.locator.PasteCalendlylink).send_keys("https://calendly.com/p3-support/covid-relief-clone-3?utm_campaign=Client")
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageUp()
          self.find_element(*self.locator.Eventname).click()
          self.find_element(*self.locator.Eventname).clear()
          self.wait_element(*self.locator.Eventname)
          self.scrollPageDown()
          self.scrollPageDown()
          self.scrollPageDown()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)
          self.find_element(*self.locator.Cancelbutton).click()

    def add_AllFields(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys("Created today")
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.MinVolunteer).send_keys(minVolunteer)
          self.find_element(*self.locator.MaxVolunteer).send_keys(maxVolunteer)
          self.find_element(*self.locator.PasteCalendlylink).send_keys("https://calendly.com/p3-support/covid-relief-clone-3?utm_campaign=Client")
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.find_element(*self.locator.EventPrereq).send_keys("test test ")
          self.find_element(*self.locator.WhyVolunteer).send_keys("test test ")
          self.wait_element(*self.locator.SelectCauses)
          self.find_element(*self.locator.SelectCauses).click()
          self.find_element(*self.locator.SearchCauses).send_keys(causeEnvironment)
          self.selectOptionfromDropDown2(*self.locator.SelectCauseEnvironment)
          self.find_element(*self.locator.SelectCauses).click()
          self.scrollPageDown()
          self.wait_element(*self.locator.SelectBeneficiaries)
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.scrollPageDown()
          self.find_element(*self.locator.EnterItineraryDuration1).send_keys("15")
          self.find_element(*self.locator.EnterItineraryTitle1).send_keys("Introduction ")
          # time.sleep(3)
          self.wait_element(*self.locator.UploadDeckFile)
          # time.sleep(3)
          # self.find_element(*self.locator.UploadDeckFile).click()
          # time.sleep(3)
          # self.wait_element(*self.locator.AttachDeckFile)
          # self.find_element(*self.locator.AttachDeckFile).send_keys("C://Users/shefali/Desktop/Deck_File_for_Testing.txt")
          # time.sleep(3)
          # self.wait_element(*self.locator.DeckFileUploadedSuccessBanner)
          # self.wait_element(*self.locator.ConfirmDeckFile)
          # time.sleep(5)
          # self.find_element(*self.locator.ConfirmDeckFile).click()
          # time.sleep(3)
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)
          time.sleep(10)

    def change_DefaultSetting(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.ParticipantFormatIndividual).click()
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.Deselectcountry).click()
          self.find_element(*self.locator.Selectcountry).click()
          self.find_element(*self.locator.Selectcountry).send_keys("Spain")
          self.wait_element(*self.locator.EventCounrtySpain)
          self.selectOptionfromDropDown2(*self.locator.EventCounrtySpain)
          self.find_element(*self.locator.Selectcountry).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.ChampionsPortalNo).click()
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)


    def add_MultipleFields(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.ParticipantFormatIndividual).click()
          self.find_element(*self.locator.EventType_Event).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.ChangeEventType) #-------- name of variable
          self.find_element(*self.locator.ChangeEventType).click()
          self.find_element(*self.locator.EventType_Event).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          # self.find_element(*self.locator.Selectlanguage_Event).click()
          # self.find_element(*self.locator.SelectlanguageArabic).click()
          # self.find_element(*self.locator.Searchlanguage_Event).send_keys("Spanish")
          # self.wait_element(*self.locator.EventLanguageSpanish)
          # self.selectOptionfromDropDown2(*self.locator.EventLanguageSpanish)
          # self.find_element(*self.locator.Selectlanguage_Event).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.ChangeClientType)
          self.find_element(*self.locator.ChangeClientType).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.Selectcountry).click()
          self.find_element(*self.locator.Selectcountry).send_keys("Spain")
          self.wait_element(*self.locator.EventCounrtySpain)
          self.selectOptionfromDropDown2(*self.locator.EventCounrtySpain)
          self.find_element(*self.locator.Selectcountry).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.ChangeNonProfit)
          self.find_element(*self.locator.ChangeNonProfit).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.scrollPageDown()
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.scrollPageDown()
          self.wait_element(*self.locator.SelectCauses)
          self.find_element(*self.locator.SelectCauses).click()
          self.find_element(*self.locator.SelectCauseAnimalWelfare).click()
          self.find_element(*self.locator.SelectCauseEducation).click()
          self.find_element(*self.locator.SearchCauses).send_keys(causeEnvironment)
          self.selectOptionfromDropDown2(*self.locator.SelectCauseEnvironment)
          self.find_element(*self.locator.SelectCauses).click()
          self.scrollPageDown()
          self.wait_element(*self.locator.SelectBeneficiaries)
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.find_element(*self.locator.SelectBeneficiaryAdults).click()
          self.find_element(*self.locator.SelectBeneficiaryPatients).click()
          self.find_element(*self.locator.SelectBeneficiaries).click()
          self.scrollPageDown()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)

    def add_MandatoryFields(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click
          self.is_Disabled(*self.locator.Submitbutton)

    def add_Itinerary(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.scrollPageDown()
          self.scrollPageDown()
          self.find_element(*self.locator.EnterItineraryDuration1).send_keys("15")
          self.find_element(*self.locator.EnterItineraryTitle1).send_keys("Introduction ")
          self.find_element(*self.locator.AddItinerary).click()
          self.find_element(*self.locator.EnterItineraryDuration2).send_keys("20")
          self.find_element(*self.locator.EnterItineraryTitle2).send_keys("Agenda ")
          # self.find_element(*self.locator.DeleteItineraryDetails).click()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)
          time.sleep(3)


    def click_CancelButton(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.wait_element(*self.locator.Submitbutton)
          self.wait_element_to_be_clickable(*self.locator.Submitbutton)
          time.sleep(2)
          self.wait_element(*self.locator.Cancelbutton)
          self.find_element(*self.locator.Cancelbutton).click()
          time.sleep(3)

    def add_validDatatype(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.MinVolunteer).send_keys("-6")
          self.find_element(*self.locator.MaxVolunteer).send_keys("8.25")
          self.find_element(*self.locator.PasteCalendlylink).send_keys("Hello world ")
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test 32 90% #volunteer")
          self.scrollPageDown()
          self.find_element(*self.locator.EventPrereq).send_keys("test 32 90% #volunteer")
          self.find_element(*self.locator.WhyVolunteer).send_keys("self.generateRandomString(40)")
          self.scrollPageDown()
          self.find_element(*self.locator.EnterItineraryDuration1).send_keys("hii")
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)
          time.sleep(3)
          self.find_element(*self.locator.Cancelbutton).click()
          time.sleep(3)

    def add_oneFieldInItinerary(self):
          self.driver.get('https://stagingvvapp.goodera.com')
          self.wait_element(*self.locator.EmailLogin)
          self.find_element(*self.locator.EmailLogin).send_keys(login)
          self.find_element(*self.locator.PasswordLogin).send_keys(password)
          # self.find_element(*self.locator.RemembermeToggle).click()
          self.find_element(*self.locator.SubmitLogin).click()
          self.wait_element(*self.locator.LoginSuccessBanner)
          time.sleep(3)  # necessary
          self.wait_element(*self.locator.AddEvent)
          self.find_element(*self.locator.AddEvent).click()
          self.wait_element(*self.locator.Eventname)
          time.sleep(3)  # necessary
          self.find_element(*self.locator.Eventname).send_keys(self.generateRandomString(5))
          self.find_element(*self.locator.EventType_Event).click()
          self.scrollPageDown()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectClient).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.find_element(*self.locator.SelectNonProfit).click()
          self.find_element(*self.locator.SelectAudioRecording).click()
          self.wait_element(*self.locator.UploadEventImage)
          self.find_element(*self.locator.UploadEventImage).send_keys("C://Users/shefali/Desktop/image2.png")
          self.wait_element(*self.locator.SubmitEventImage)
          self.find_element(*self.locator.SubmitEventImage).click()
          self.wait_element(*self.locator.ImageUploadedSuccessBanner)
          self.find_element(*self.locator.EventDescription).send_keys("test test test ")
          self.scrollPageDown()
          self.find_element(*self.locator.EnterItineraryDuration1).send_keys("15")
          self.find_element(*self.locator.Submitbutton).click()
          self.is_Disabled(*self.locator.Submitbutton)










