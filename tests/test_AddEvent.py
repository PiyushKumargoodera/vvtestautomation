import unittest
from tests.base_test import BaseTest
from pages.EventPage import *
from utils.test_cases import test_cases


class TestAddEventPage(BaseTest):


    def test_AddEvent(self):
        page = EventPage(self.driver)
        test_add_Event=page.add_event()

    def test_AddEvent_removeMandateFields(self):
        page = EventPage(self.driver)
        test_remove_MandateFields=page.remove_MandateFields()

    def test_AddEvent_allFields(self):
         page = EventPage(self.driver)
         test_add_AllFields=page.add_AllFields()

    def test_AddEvent_changeDefaultSetting(self):
          page = EventPage(self.driver)
          test_change_DefaultSetting=page.change_DefaultSetting()

    def test_AddEvent_addMultipleFields(self):
          page = EventPage(self.driver)
          test_add_MultipleFields=page.add_MultipleFields()

    def test_AddEvent_addMandatoryFields(self):
          page = EventPage(self.driver)
          test_add_MandatoryFields=page.add_MandatoryFields()

    def test_AddEvent_addItinerary(self):
          page = EventPage(self.driver)
          test_add_Itinerary=page.add_Itinerary()

    def test_AddEvent_clickCancelButton(self):
          page = EventPage(self.driver)
          test_click_CancelButton = page.click_CancelButton()

    def test_AddEvent_addValidDatatype(self):
          page = EventPage(self.driver)
          test_add_validDatatype = page.add_validDatatype()

    def test_AddEvent_addOneFieldInItinerary(self):
          page = EventPage(self.driver)
          test_add_oneFieldInItinerary=page.add_oneFieldInItinerary()





    
        # def test_Event_search(self):
        #
        #  page = EventPage(self.driver)
        #  Event_Listing=page.event_Listing()
        #
        #
        # def test_championsPageSync(self):
        #        page = EventPage(self.driver)
        #        sync_Event=page.VerifyEventChampionsPage()