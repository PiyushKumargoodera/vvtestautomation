from tests.base_test import BaseTest
from pages.home_page import *


class TestEventListing(BaseTest):

    def test_EventListing_searchByEventName(self):
        page =HomePage(self.driver)
        test_searchByEventName = page.searchByEventName()

    def test_EventListing_searchByEventNameandCountry(self):
        page = HomePage(self.driver)
        test_searchByEventNameandCountry=page.searchByEventNameandCountry()

    def test_EventListing_searchByAll(self):
        page = HomePage(self.driver)
        test_searchByAll=page.searchByAll()

    ###error 120
    def test_EventListing_edit_EventDetails(self):
        page = HomePage(self.driver)
        test_edit_EventDetails = page.edit_EventDetails()

    def test_EventListing_remove_UserSelection(self):
        page = HomePage(self.driver)
        test_remove_UserSelection=page.remove_UserSelection()

    def test_EventListing_is_Clickable(self):
        page = HomePage(self.driver)
        test_is_Clickable=page.is_Clickable()

    def test_EventListing_check_eventDeckFile(self):
        page = HomePage(self.driver)
        test_check_eventDeckFile=page.check_eventDeckFile()

