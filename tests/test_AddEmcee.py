from  unittest import *
from tests.base_test import BaseTest
from pages.EmceePage import *
from utils.test_cases import test_cases



class TestAddEmceePage(BaseTest):


    def test_AddEmcee_MandateFields(self):
        page = EmceePage(self.driver)
        test_MandateFields = page.add_EMCEE_MandateField()

    def test_AddEmcee_AllFields(self):
        page = EmceePage(self.driver)
        test_AllFields = page.add_EMCEE_AllFields()

    def test_AddEmcee_NotMandateFields(self):
        page = EmceePage(self.driver)
        test_NotMandateFields = page.add_EMCEE_NotMandateFields()

    def test_AddEmcee_ClickDisabled(self):
        page = EmceePage(self.driver)
        test_ClickDisabled = page.add_Emcee_ClickDisabled()

    def test_AddEmcee_LongData(self):
        page = EmceePage(self.driver)
        test_LongData = page.add_Emcee_LongData()

    def test_AddEmcee_clearCountryOrLanguage(self):
        page = EmceePage(self.driver)
        test_clear_CountryOrLanguage = page.add_Emcee_clear_CountryOrLanguage()

    def test_AddEmcee_removeMandateData(self):
        page = EmceePage(self.driver)
        #  The submit Emcee button is enabled here ####Error####
        test_removeMandateData = page.add_Emcee_remove_MandateData()

    def test_EditEmcee(self):
        page = EmceePage(self.driver)
        test_EditEmcee = page.EditEmcee()




    









