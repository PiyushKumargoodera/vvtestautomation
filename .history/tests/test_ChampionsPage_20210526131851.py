import unittest
from tests.base_test import BaseTest
from pages.main_page import *
from utils.test_cases import test_cases


# I am using python unittest for asserting cases.
# In this module, there should be test cases.
# If you want to run it, you should type: python <module-name.py>

class TestChampionsPage(BaseTest):

    def test_page_load(self):
        print("\n" + str(test_cases(0)))
        page = MainPage(self.driver)
        self.assertTrue(page.check_page_loaded())

    def test_click_schedule(self):
        print("\n" + str(test_cases(1)))
        page = MainPage(self.driver)
        schedule_click = page.schedule_event()
        #self.assertIn("iPhone", search_result)
    def test_dateandtime_schedule(self):
        print("\n" + str(test_cases(1)))
        page = MainPage(self.driver)
        schedule_slot = page.select_dateandtime()

    

    def test_country_button(self):
        print("\n" + str(test_cases(2)))
        page = MainPage(self.driver)
        change_language = page.click_country_change()
        

    def test_font_in_button(self):
        print("\n" + str(test_cases(3)))
        page = MainPage(self.driver)
        login_page = page.click_sign_in_button()
        self.assertIn("ap/signin", login_page.get_url())

    def test_language_button(self):
        print("\n" + str(test_cases(4)))
        main_page = MainPage(self.driver)
        change_country = main_page.click_language_change()
        

    def test_faq_button(self):
        print("\n" + str(test_cases(5)))
        main_page = MainPage(self.driver)
        click_faq = main_page.click_FAQ()
        #result = login_page.login_with_in_valid_user("invalid_user")
        #self.assertIn("There was a problem with your request", result)
    


    def test_chatbox_button(self):
    
     print("\n" + str(test_cases(5)))
     main_page = MainPage(self.driver)
     click_chat = main_page.click_chat_box()